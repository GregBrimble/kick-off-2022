import { prettyHTML } from "./htmlResponse";

export const onRequest = async ({ request, env }) => {
  let posts = (await env.KICK_OFF.get("posts", "json")) || [];

  if (request.method.toLowerCase() === "post") {
    const formData = await request.formData();
    const newPost = formData.get("post");
    if (newPost) {
      posts.push(newPost);
      await env.KICK_OFF.put("posts", JSON.stringify(posts));
    } else {
      await env.KICK_OFF.put("posts", JSON.stringify([]));
    }
  }

  return new Response(
    ...prettyHTML(`
<ul>
  ${posts.map((post) => `<li>${post}</li>`).join("")}
</ul>
<form method="POST">
  <input type="text" name="post">
  <br />
  <button type="submit" class="mt-2 px-4 py-2 bg-blue-100 rounded-lg">Add</button>
</form>
  `)
  );
};
