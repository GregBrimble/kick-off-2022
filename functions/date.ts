import { prettyHTML } from "./htmlResponse";

export const onRequest = () =>
  new Response(
    ...prettyHTML(
      `<p>The time according to Cloudflare is ${new Date().toISOString()}.</p>
      <p><a href="/kv">KV Demo &rarr;</a></p>`
    )
  );
