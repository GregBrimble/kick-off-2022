export const prettyHTML = (
  children: string
): [string, { headers: Record<string, string> }] => [
  `<!DOCTYPE html>
<html>
  <head>
    <title>Kick-off 2022</title>
    <script src="./tailwind.js"></script>
  </head>
  <body class="flex h-screen items-center">
    <main class="prose prose-2xl mx-auto">
      ${children}
    </main>
  </body>
</html>
`,
  { headers: { "Content-Type": "text/html" } },
];
