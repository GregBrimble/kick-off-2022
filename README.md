# Kick-off 2022

![./public/Pages.svg](./public/Pages.svg)

## Q4 2021

- GitLab

- `_headers` and `_redirects`

- Functions incl. full-stack frameworks:

  - [SvelteKit](https://summit.pages.dev/)

  - [Remix](https://remix.pages.dev/)

  - [Qwik](https://qwik-app-cf.pages.dev/)

- New build infrastructure
